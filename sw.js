importScripts('/dist/idb.js');
importScripts('/dist/utility.js');

var CACHE_STATIC_NAME = 'static-v13';
var CACHE_DYNAMIC_NAME = 'dynamic-v1';
var STATIC_FILES = [
  '/',
  '/index.html',
  '/dist/cloeportaldiseno.css',
  '/dist/img/favicon.png',
  '/dist/cloeportaldiseno.js',
  '/dist/img/design-book.svg',
  '/dist/img/design-dashboard.svg',
  '/dist/img/design-plan.svg',
  '/dist/img/logo.png',
  '/dist/img/logout.svg',
  '/dist/img/pdf-file.svg',
  '/dist/img/product-line.svg',
  '/dist/img/suppliers.svg',
  '/dist/img/users.svg',
  '/dist/idb.js',
  '/dist/utility.js',
  'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700',
];
var token = "";


self.addEventListener('install', function (event) {
    console.log('[Service Worker] Installing Service Worker ...', event);
    event.waitUntil(
      caches.open(CACHE_STATIC_NAME)
        .then(function (cache) {
          console.log('[Service Worker] Precaching App Shell');
          cache.addAll(STATIC_FILES);
        })
    )
  });
  
  self.addEventListener('activate', function (event) {
    console.log('[Service Worker] Activating Service Worker ....', event);
    event.waitUntil(
      caches.keys()
        .then(function (keyList) {
          return Promise.all(keyList.map(function (key) {
            if (key !== CACHE_STATIC_NAME && key !== CACHE_DYNAMIC_NAME) {
              console.log('[Service Worker] Removing old cache.', key);
              return caches.delete(key);
            }
          }));
        })
    );
    return self.clients.claim();
  });


function isInArray(string, array) {
    var cachePath;
    if (string.indexOf(self.origin) === 0) { // request targets domain where we serve the page from (i.e. NOT a CDN)
      // console.log('matched ', string);
      cachePath = string.substring(self.origin.length); // take the part of the URL AFTER the domain (e.g. after localhost:8080)
    } else {
      cachePath = string; // store the full request (for CDNs)
    }
    return array.indexOf(cachePath) > -1;
}
  
/*self.addEventListener('fetch', function (event) {

  var url = 'https://pwagram-99adf.firebaseio.com/posts';
  if (event.request.url.indexOf(url) > -1) {
    event.respondWith(fetch(event.request)
      .then(function (res) {
        var clonedRes = res.clone();
        clearAllData('posts')
          .then(function () {
            return clonedRes.json();
          })
          .then(function (data) {
            for (var key in data) {
              writeData('posts', data[key])
            }
          });
        return res;
      })
    );
  } else if (isInArray(event.request.url, STATIC_FILES)) {
    event.respondWith(
      caches.match(event.request)
    );
  } else {
    event.respondWith(
      caches.match(event.request)
        .then(function (response) {
          if (response) {
            return response;
          } else {
            return fetch(event.request)
              .then(function (res) {
                return caches.open(CACHE_DYNAMIC_NAME)
                  .then(function (cache) {
                    // trimCache(CACHE_DYNAMIC_NAME, 3);
                    cache.put(event.request.url, res.clone());
                    return res;
                  })
              })
              /*.catch(function (err) {
                return caches.open(CACHE_STATIC_NAME)
                  .then(function (cache) {
                    if (event.request.headers.get('accept').includes('text/html')) {
                      return cache.match('/offline.html');
                    }
                  });
              }); -->
          }
        })
    );
  }
}); */

self.addEventListener('sync', function(event) {
  console.log('[Service Worker] Background syncing', event);
  if (event.tag === 'sync-new-posts') {
    console.log('[Service Worker] Syncing new Posts');
    event.waitUntil(
      readAllData('user')
        .then(function(dataUser) {
          console.log("dataUser", dataUser)
          token = dataUser[0].authorization

          readAllData('sync-products-line')
            .then(function(data) {
                console.log("product line")

                for (var dt of data) {
                  fetch('https://apipruebashojaruta.oemoda.com/api/lines', {
                    method: 'POST',
                    headers: {
                      'Content-Type': 'application/json',
                      'Accept': 'application/json',
                      'Authorization':  token,
                    },
                    body: JSON.stringify(dt)
                  })
                    .then(function(res) {
                      console.log('Sent data', res);
                      if (res.ok) {
                        res.json()
                          .then(function(resData) {
                            deleteItemFromData('sync-products-line', dt.id);
                            
                            console.log("res2", "Dentro aquí")

                            // readItemData('sync-models', dt.id)
                            readAllData('sync-models')
                              .then(function(data) {
                                console.log("models", data)

                                for (var dtm of data) {
                                  console.log("dtm", dtm)
                                  let dataToSend = dtm
                                  console.log("dataToSend", dataToSend)

                                  if(dt.id != dtm.data.line_id)
                                    return
                                  
                                  dataToSend.data.line_id = resData.data.line_id
                                  console.log("dataToSend", dataToSend)

                                  fetch('https://apipruebashojaruta.oemoda.com/api/models', {
                                    method: 'POST',
                                    headers: {
                                      'Content-Type': 'application/json',
                                      'Accept': 'application/json',
                                      'Authorization':  token,
                                    },
                                    body: JSON.stringify(dataToSend.data)
                                  })
                                  .then(function(redDataModel) {
                                    console.log('Sent data model', redDataModel);
                                    if (redDataModel.ok) {
                                      redDataModel.json()
                                        .then(function(res) {
                                          deleteItemFromData('sync-products-line', dataToSend.id);
                                        })
                                    }
                                  })
                                  .catch(err => console.log('Error while sending data', err));
                                }
                              })
                              .catch(function(err) {
                                console.log('Error while sending data', err);
                              })
                          });
                      }
                    })
                    .catch(function(err) {
                      console.log('Error while sending data', err);
                    }); 
                }
            })
        })
        .catch(function(err) {
          console.log('Error while getting user data', err);
        })
    );
  }
});