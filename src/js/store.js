import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  userInfo: localStorage.role ? localStorage.role : false
}

const mutations = {
  SET_USER_INFO (state, value) {
    state.userInfo = value
  }
}

// call for get value
const getters = {
  getUserInfo (state) {
    return state.userInfo
  }
}

// call for change state
const actions = {
  setUserInfo (state, value) {
    state.commit('SET_USER_INFO', value)
  }
}

export default new Vuex.Store({
  state,
  mutations,
  getters,
  actions
})
