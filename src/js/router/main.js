import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from './../components/Login/Login.vue'
import Home from './../components/Home/Home.vue'
import DesignDashboard from './../components/DesignDashboard/DesignDashboard.vue'
import UneDesignDashboard from './../components/DesignDashboard/UneDesignDashboard.vue'
import UserDesignDashboard from './../components/DesignDashboard/UserDesignDashboard.vue'
import AddEditDesign from './../components/DesignPlan/AddEditDesign.vue'
import AddEditDesignNew from './../components/DesignPlan/AddEditDesign.vue'
import DesignBook from './../components/DesignBook/DesignBook.vue'
import Users from './../components/Users/Users.vue'
import SuppliersList from './../components/Suppliers/SuppliersList.vue'
import Suppliers from './../components/Suppliers/Suppliers.vue'
import ProductLineNew from './../components/ProductLine/ProductLineNew.vue'
import ProductLineEdit from './../components/ProductLine/ProductLineEdit.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '*',
    component: Home
  },
  {
    path: '/login',
    component: Login,
    meta: {
      forVisitors: true
    }
  },
  {
    path: '/design-dashboard',
    component: DesignDashboard,
    params: true,
    meta: {
      forAuth: true
    }
  },
  {
    path: '/design-dashboard/une/:id',
    name: 'design-dashboard-une',
    component: UneDesignDashboard,
    params: true,
    meta: {
      forAuth: true
    }
  },
  {
    path: '/design-dashboard/user/:id',
    name: 'design-dashboard-user',
    component: UserDesignDashboard,
    params: true,
    meta: {
      forAuth: true
    }
  },
  {
    path: '/design-plan/create',
    component: AddEditDesignNew,
    meta: {
      forAuth: true
    }
  },
  {
    path: '/design-plan/edit/:une/:user',
    component: AddEditDesign,
    meta: {
      forAuth: true
    }
  },
  {
    path: '/design-book/:id',
    component: DesignBook,
    meta: {
      forAuth: true
    }
  },
  {
    path: '/design-book',
    component: DesignBook,
    meta: {
      forAuth: true
    }
  },
  {
    path: '/users',
    component: Users,
    meta: {
      forAuth: true
    }
  },
  {
    path: '/suppliers',
    component: SuppliersList,
    meta: {
      forAuth: true
    }
  },
  {
    path: '/suppliers/:id',
    component: Suppliers,
    meta: {
      forAuth: true
    }
  },
  {
    path: '/suppliers/create',
    component: Suppliers,
    meta: {
      forAuth: true
    }
  },
  {
    path: '/product-line',
    component: ProductLineNew,
    meta: {
      forAuth: true
    }
  },
  {
    path: '/product-line/edit/:id',
    component: ProductLineEdit,
    meta: {
      forAuth: true
    }
  },
  {
    path: '/product-line/edit/:id/new',
    component: ProductLineEdit,
    meta: {
      forAuth: true
    }
  }
]

export default new VueRouter({
  mode: 'history',
  routes
})
