import Vue from 'vue'
import router from './router/main'
import Auth from './auth/Auth.js'
import store from './store'

import HeaderContent from './components/Includes/HeaderContent.vue'
import SidebarContent from './components/Includes/SidebarContent.vue'

window.is = require('is_js')
window.axios = require('axios')
window.$ = window.jQuery = require('jquery')
window.APP_NAME = 'Cloe Design Portal'
window.URL_SERVER = 'https://89389f95.ngrok.io'
window.CLIENT_SECRET = 'WaE585pdQ0v186Y4zAicAMDgAJTU7IwRMxsXPwhT'

if (window.location.hostname == 'pruebashojaruta.oemoda.com') {
  window.URL_SERVER = 'https://apipruebashojaruta.oemoda.com'
}

if (window.location.hostname == 'designbook.oemoda.com') {
  window.URL_SERVER = 'https://apidesingbook.oemoda.com'
}

Vue.use(Auth)

// Axios config
axios.defaults.baseURL = URL_SERVER
if (localStorage.getItem('token') != null) {
  axios.defaults.headers.common['Authorization'] =
    'Bearer ' + Vue.auth.getToken()
}
axios.defaults.headers.post['Content-Type'] = 'application/json'

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.forVisitors)) {
    if (Vue.auth.isAuthenticated()) {
      next({
        path: '/design-dashboard'
      })
    } else next()
  } else if (to.matched.some(record => record.meta.forAuth)) {
    if (!Vue.auth.isAuthenticated()) {
      next({
        path: '/login'
      })
    } else if (to.fullPath == '/design-dashboard' && isDesigner()) {
      next({
        path: '/design-dashboard/user/' + uid()
      })
    } else next()
  } else next()
})

window.isDesigner = function () {
  let designer = localStorage.getItem('role') || ''
  return designer == 'designer'
}

window.isAdmin = function () {
  let admin = localStorage.getItem('role') || ''
  return admin == 'admin'
}

window.uid = function () {
  let uid = localStorage.getItem('id') || ''
  return uid
}

const content = new Vue({
  el: '#app',
  components: {
    HeaderContent,
    SidebarContent
  },
  store,
  router,
  data: {
    error_msg: ''
  },
  methods: {
    openSidebar () {
      let sidebar = document.querySelector('.sidebar')
      let navToggle = document.querySelector('.nav-toggle')

      sidebar.classList.toggle('open-sidebar')
      navToggle.classList.toggle('active')
    }
  },
  computed: {
    url () {
      return this.$route.path
    },

    hasToShowHeader () {
      let show_header = false

      switch (this.url) {
        case '/design-dashboard':
          show_header = true
          break
        case '/design-book':
          show_header = true
          break
        case '/users':
          show_header = true
          break
        case '/suppliers':
          show_header = true
          break
        case '/product-line':
          show_header = true
          break
        default:
          if (this.url.split('/')[1] == 'suppliers') show_header = true

          if (this.url.split('/')[1] == 'design-plan') show_header = true

          if (this.url.split('/')[1] == 'design-dashboard') show_header = true

          if (this.url.split('/')[1] == 'design-plan') show_header = true

          if (this.url.split('/')[1] == 'product-line') show_header = true

          if (this.url.split('/')[1] == 'design-book') show_header = true

          if (this.url.split('/')[1] == 'une-dashboard') show_header = true
          break
      }

      return show_header
    }
  },
  watch: {
    error_msg () {
      if (content.error_msg != '') {
        setTimeout(function () {
          content.error_msg = ''
        }, 4500)
      }
    }
  }
})

window.content = content

$(document).mouseup(function (e) {
  var menu_container = $('.menu')

  if (
    !menu_container.is(e.target) &&
    menu_container.has(e.target).length === 0
  ) {
    if ($('.nav-toggle').hasClass('active')) content.openSidebar()
  }
})

window.processStrJson = function (str, htmlPre, htmlPos) {
  if (str == '') return ''

  var htmlPre = htmlPre || '* '
  var htmlPos = htmlPos || '<br/>'

  if (typeof str === 'string') {
    return htmlPre + str + htmlPos
  } else if (Array.isArray(str) || typeof str === 'object') {
    var output = ''
    for (var objt in str) {
      for (var i = 0; i <= str[objt].length - 1; i++) {
        var value = str[objt][i]

        if (value != '') {
          output += htmlPre + value + htmlPos
        }
      }
    }

    return output
  }
  return ''
}

window.delayKeyup = function () {
  let timer = 0
  return function (callback, ms) {
    clearTimeout(timer)
    timer = setTimeout(callback, ms)
  }
}
