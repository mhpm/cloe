// importScripts('/dist/idb.js');

var dbPromise = idb.open('cloe-store', 5, function (db) {
  if (!db.objectStoreNames.contains('user')) {
    db.createObjectStore('user', { keyPath: 'id' })
  }
  if (!db.objectStoreNames.contains('products-line')) {
    db.createObjectStore('products-line', { keyPath: 'id' })
  }
  if (!db.objectStoreNames.contains('seasons')) {
    db.createObjectStore('seasons', { keyPath: 'id' })
  }
  if (!db.objectStoreNames.contains('trends')) {
    db.createObjectStore('trends', { keyPath: 'id' })
  }
  if (!db.objectStoreNames.contains('clients')) {
    db.createObjectStore('clients', { keyPath: 'id' })
  }
  if (!db.objectStoreNames.contains('families')) {
    db.createObjectStore('families', { keyPath: 'id' })
  }
  if (!db.objectStoreNames.contains('prices')) {
    db.createObjectStore('prices', { keyPath: 'id' })
  }
  if (!db.objectStoreNames.contains('shapes')) {
    db.createObjectStore('shapes', { keyPath: 'id' })
  }
  if (!db.objectStoreNames.contains('suppliers')) {
    db.createObjectStore('suppliers', { keyPath: 'id' })
  }
  if (!db.objectStoreNames.contains('sub-brands')) {
    db.createObjectStore('sub-brands', { keyPath: 'id' })
  }
  if (!db.objectStoreNames.contains('unes')) {
    db.createObjectStore('unes', { keyPath: 'id' })
  }
  if (!db.objectStoreNames.contains('colors')) {
    db.createObjectStore('colors', { keyPath: 'id' })
  }
  if (!db.objectStoreNames.contains('sync-products-line')) {
    db.createObjectStore('sync-products-line', { keyPath: 'id' })
  }
  if (!db.objectStoreNames.contains('design-une')) {
    db.createObjectStore('design-une', { keyPath: 'id' })
  }
  if (!db.objectStoreNames.contains('design-product-line')) {
    db.createObjectStore('design-product-line', { keyPath: 'id' })
  }
  if (!db.objectStoreNames.contains('dimensions')) {
    db.createObjectStore('dimensions', { keyPath: 'id' })
  }
  if (!db.objectStoreNames.contains('sizes')) {
    db.createObjectStore('sizes', { keyPath: 'id' })
  }
  if (!db.objectStoreNames.contains('sync-models')) {
    db.createObjectStore('sync-models', { keyPath: 'id' })
  }
})

function writeData (st, data) {
  return dbPromise.then(function (db) {
    var tx = db.transaction(st, 'readwrite')
    var store = tx.objectStore(st)
    store.put(data)
    return tx.complete
  })
}

function readAllData (st) {
  return dbPromise.then(function (db) {
    var tx = db.transaction(st, 'readonly')
    var store = tx.objectStore(st)
    return store.getAll()
  })
}

function readItemData (st, id) {
  return dbPromise.then(function (db) {
    var tx = db.transaction(st, 'readonly')
    var store = tx.objectStore(st)
    return store.get(id)
  })
}

function clearAllData (st) {
  return dbPromise.then(function (db) {
    var tx = db.transaction(st, 'readwrite')
    var store = tx.objectStore(st)
    store.clear()
    return tx.complete
  })
}

function deleteItemFromData (st, id) {
  dbPromise
    .then(function (db) {
      var tx = db.transaction(st, 'readwrite')
      var store = tx.objectStore(st)
      store.delete(id)
      return tx.complete
    })
    .then(function () {
      console.log('Item deleted!')
    })
}

// mhpm
async function getUserFormated () {
  let data = await readAllData('user').then(function (data) {
    return data[0]
  })
  return data
}
